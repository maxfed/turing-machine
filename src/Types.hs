module Types where

import Graphics.Gloss.Interface.Pure.Game

--------------
-- Constants
--------------

-- Game display mode.
display :: Display
display = InWindow "Turing Machine" (1000, 700) (0, 0)

-- Background color.
bgColor :: Color
bgColor = greyN 0.9

-- Simulation steps per second.
fps :: Int
fps = 1

--Limits for display rules
maxY :: Float
maxY = 350

maxX :: Float
maxX = 500

minX_rule :: Float
minX_rule = 105

shiftX_rule :: Float
shiftX_rule = 30

shiftY_rule :: Int
shiftY_rule = 50

tape_startX :: Float
tape_startX = 10-maxX

tape_distX :: Float
tape_distX = 50

--------------
-- Data types.
--------------
type State = String
data Direction = R | L | S
-- Config for alphabet.
data Alphabet = Alphabet
  { null :: String -- blank symbol
  , letters :: [String] -- allowed symbols
  }

-- текущая конфигурация
data Status = Status
  { tape :: [String]
  , pointer :: Int
  , state :: State
  , is_stop :: Bool
  }

type Rules = [Rule]
data Rule = Rule
  { state0 :: State -- текущее состояние
  , symb0  :: String -- текущий символ
  , symb1  :: String -- символ, на который надо заменить
  , state1 :: State -- состояние, в которое надо перейти
  , direct :: Direction -- в какую сторону надо сдвинуть головку
  }
data StateGame = Pause | Stop | Run deriving (Show, Eq)

data AppState = AppState -- состояние приложения
  { status :: Status
  , rules :: Rules
  , alphabet :: Alphabet
  , state_game :: StateGame -- 'pause' / 'stop' / 'run'
  , status_0 :: Status
  }

{-# LANGUAGE RecordWildCards #-}
module MT
    ( run
    ) where

import Types
import Parsers
import System.IO
import Graphics.Gloss.Interface.Pure.Game

------------------
-- Pure functions.
------------------

-- вычислить результат программы полностью
go :: Alphabet -> Status -> Rules -> String
go Alphabet{..} Status{..} rules | is_stop = unwords (prepTape tape null)
                         | otherwise = go Alphabet{..} (go_one Alphabet{..} Status{..} rules) rules

-- выполнить один шаг МТ
go_one :: Alphabet -> Status -> Rules -> Status
go_one Alphabet{..} Status{..} rules = acceptRule Status{..} (findRule Status{..} rules) Alphabet{..}

-- найти подходящее правило под текущее состояние
findRule :: Status -> Rules -> Rule
findRule Status{..} (Rule{..}:rs) | (tape !! pointer)==symb0 && state==state0 = Rule{..}
                                  | otherwise = findRule Status{..} rs

-- применить правило к текущему состоянию
acceptRule :: Status -> Rule -> Alphabet -> Status
acceptRule Status{..} Rule{..} Alphabet{..} = pointShift null st direct
                                                where is_s = state1 == "STOP"
                                                      st = Status (changeTape tape pointer symb1) pointer state1 is_s
                                              
-- изменение в ленте текущего символа                                  
changeTape :: [String] -> Int -> String -> [String]
changeTape tape n symb | symb=="" = tape
                       | otherwise = start ++ (symb:end)
                                      where start = take n tape
                                            end = drop (n+1) tape
                                  
-- сдвиг головки по ленте
pointShift :: String -> Status -> Direction -> Status
pointShift null Status{..} dir = case dir of
  S -> Status{..}
  L -> if pointer > 0 then Status tape (pointer-1) state is_stop else Status (null:tape) pointer state is_stop
  R -> if pointer < ((length tape)-1) then Status tape (pointer+1) state is_stop else Status (tape++[null]) (pointer+1) state is_stop

-- удаление из ленты с краёв пустые символы
prepTape :: [String] -> String -> [String]
prepTape tape null = reverse (dropWhile (\x -> x==null) (reverse tape1))
                      where tape1 = dropWhile (\x -> x==null) tape
                     

-- Draw a picture
drawApp :: AppState -> Picture
drawApp (AppState Status{..} rules Alphabet{..} state_game status_0) = Pictures ([top_tape, down_tape, right_tape, lRuleLine, hline1_rule, hline2_rule, str_rule] ++ ruls_pics ++ tape_pics ++ pointer_lines ++ current_rule_pics)
  where -- [-720;720] x [-450;450] 
    minY_rule = maxY-50-(fromIntegral (length rules)*(fromIntegral shiftY_rule))
    lRuleLine = Color blue $ line [(minX_rule,minY_rule), (minX_rule,maxY)]
    hline1_rule = Color blue $ line [(minX_rule,maxY-40), (maxX,maxY-40)]
    hline2_rule = Color blue $ line [(minX_rule,minY_rule), (maxX,minY_rule)]
    str_rule = Translate (minX_rule + 160) (maxY-35) $ Color black $ scale 0.3 0.3 (Text "Rules")
    ruls_pics = rules2pics rules Status{..}
    top_tape = Color (greyN 0.4) $ line [(-maxX,28), (minX_rule,28)]
    down_tape = Color (greyN 0.4) $ line [(-maxX,-52), (minX_rule,-52)]
    right_tape = Color (greyN 0.4) $ line [(minX_rule,28), (minX_rule,-52)]
    pointer_line1 = Color red $ line [(tape_startX + (fromIntegral pointer)*tape_distX + 20, 58), (tape_startX + (fromIntegral pointer)*tape_distX + 20, 28)]
    pointer_line2 = Color red $ line [(tape_startX + (fromIntegral pointer)*tape_distX + 10, 48), (tape_startX + (fromIntegral pointer)*tape_distX + 20, 28)]
    pointer_line3 = Color red $ line [(tape_startX + (fromIntegral pointer)*tape_distX + 30, 48), (tape_startX + (fromIntegral pointer)*tape_distX + 20, 28)]
    pointer_lines = [pointer_line1, pointer_line2, pointer_line3]
    tape_pics = tapes2pics tape state
    horizontCurRule = Color blue $ line [((1-maxX), 150), ((150-maxX), 150)]
    curRuleText1 = Translate (10-maxX) 210 $ scale 0.3 0.3 $ Text "Current"
    curRuleText2 = Translate (35-maxX) 160 $ scale 0.3 0.3 $ Text "state"
    curStateText = Translate (30-maxX) 110 $ scale 0.3 0.3 $ Color col_state $ Text state
      where col_state | is_stop = red
                      | otherwise = black
    textStateGamePic | state_game == Pause = Translate (210-maxX) (maxY-200) $ Color red $ Text (show state_game)
                     | state_game == Stop = Translate (270-maxX) (maxY-200) $ Color red $ Text (show state_game)
                     | otherwise = Text ""
    current_rule_pics = textStateGamePic : curStateText : curRuleText1 : curRuleText2 : horizontCurRule : (map (\pic -> Color blue pic) $ makeRectangle (1-maxX) (150-maxX) 250 100)

-- создание четырехугольника по 4-м точкам
makeRectangle :: Float -> Float -> Float -> Float -> [Picture]
makeRectangle x1 x2 y1 y2 = [l1, l2, l3, l4]
  where
    l1 = line[(x1, y1), (x1, y2)]
    l2 = line[(x1, y2), (x2, y2)]
    l3 = line[(x2, y2), (x2, y1)]
    l4 = line[(x2, y1), (x1, y1)]
rule2pic :: Rule -> Float -> Bool -> Picture
rule2pic r n cmp = Translate (minX_rule + shiftX_rule) (maxY-70-n*(fromIntegral shiftY_rule)) $ Color col $ scale 0.25 0.25 $ Text (rule2str r)
                    where col = if cmp then red else black

-- создание рисунка с правилами                 
rules2pics :: Rules -> Status -> [Picture]
rules2pics rules st = rules2pics1 rules st 0
  where
    rules2pics1 :: Rules -> Status -> Float -> [Picture]
    rules2pics1 (r:rs) Status{..} n = (rule2pic r n cmp_rules) : (rules2pics1 rs Status{..} (n+1))
      where cmp_rules | is_stop = False
                      | otherwise = (cmpRules r (findRule st rules))
    rules2pics1 [] _ _ = []

    -- сравнивание правил, если правило одно и то же, то возвращает True
    cmpRules :: Rule -> Rule -> Bool
    cmpRules (Rule st0 s0 s1 st1 dir) (Rule st0_1 s0_1 s1_1 st1_1 dir_1) = st0==st0_1 && s0==s0_1 && s1==s1_1 && st1==st1_1 && dir_str==dir_1_str
        where dir_str = dir2str dir
              dir_1_str = dir2str dir_1
    dir2str :: Direction -> String
    dir2str d = case d of
          L -> "L"
          R -> "R"
          S -> "S"

-- создание рисунка ленты
tapes2pics :: [String] -> State -> [Picture]
tapes2pics tape st = (tapes2pics_tape tape) ++ (tapes2pics_add st)
  where
    tapes2pics_tape :: [String] -> [Picture]
    tapes2pics_tape tape = tapes2pics_tape1 (map (\s -> scale 0.5 0.5 (Translate 0 (-65) (Text s))) tape) 0

    tapes2pics_tape1 :: [Picture] -> Float -> [Picture]
    tapes2pics_tape1 [] _ = []
    tapes2pics_tape1 (p:ps) n = (Translate (tape_startX + n*tape_distX) 0 p) : (tapes2pics_tape1 ps (n+1))

    tapes2pics_add :: State -> [Picture]
    tapes2pics_add st = []

-- Handle events.
handleEvent :: Event -> AppState -> AppState
handleEvent (EventKey (Char 's') Down _ _) (AppState Status{..} rules alphabet _ status_0) | not is_stop = AppState Status{..} rules alphabet Stop status_0
handleEvent (EventKey (Char 'c') Down _ _) (AppState Status{..} rules alphabet Pause status_0) = AppState Status{..} rules alphabet Run status_0
handleEvent (EventKey (Char 'p') Down _ _) (AppState Status{..} rules alphabet Run status_0) | not is_stop = AppState Status{..} rules alphabet Pause status_0
handleEvent (EventKey (SpecialKey KeyF5) Down _ _) (AppState Status{..} rules alphabet _ status_0) = AppState status_0 rules alphabet Run status_0
handleEvent _ state = state

-- Simulation step (updates nothing).
updateApp :: Float -> AppState -> AppState
updateApp _ (AppState Status{..} rules Alphabet{..} state_game status_0) | is_stop = AppState (Status (prepTape tape null) pointer state is_stop) rules Alphabet{..} state_game status_0
updateApp _ (AppState st rules alph Stop status_0) = AppState st rules alph Stop status_0
updateApp _ (AppState st rules alph Pause status_0) = AppState st rules alph Pause status_0
updateApp _ (AppState Status{..} rules Alphabet{..} Run status_0) = AppState (go_one Alphabet{..} Status{..} rules) rules Alphabet{..} Run status_0

------------------------------
-- Main function for this app.
------------------------------

run :: IO()
run = do
  putStr "Input name of a program: "
  hFlush stdout
  name_program <- getLine
  let alphabetPath = "./progs/" ++ name_program ++ ".alphabet"
      programPath = "./progs/" ++ name_program ++ ".mt"
  strAlphabet <- readFile alphabetPath
  strProg   <- readFile programPath
  putStr "Input string: "
  hFlush stdout
  strInput <- getLine
  case parseAlphabet (lines strAlphabet) of
    Nothing -> putStrLn "Parse alphabet error"
    Just (Alphabet null letters) ->
      case parseProg strProg of 
        Nothing -> putStrLn "Parse program error"
        Just (Status{..}, rules) ->
          case parseInput strInput letters of
            Nothing -> putStrLn "Parse input error"
            Just input ->
              let alph = Alphabet null letters
                  st0 = Status input pointer state is_stop
                  initState = AppState st0 rules alph Run st0 in
              case check_symbols rules (null:letters) of
                Nothing -> putStrLn "Wrong symbols in the program!"
                --_ -> putStrLn (go alph st0 rules)
                _ -> play display bgColor fps initState drawApp handleEvent updateApp


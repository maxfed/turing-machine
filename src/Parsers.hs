{-# LANGUAGE RecordWildCards #-}
module Parsers where

import Data.List.Split
import Types

--Alphabet format: 2 strings. 1 - blank symbol, 2 - list of symbols in alphabet
parseAlphabet :: [String] -> Maybe Alphabet
parseAlphabet [str1, str2] = case (parseBlank str1, parseSymbs (words str2)) of
  (Just blank, Just symbs) -> Just (Alphabet blank symbs)
  _ -> Nothing
  where
    parseBlank :: String -> Maybe String
    parseBlank str = case (length (words str)) of 
      1 -> Just (head (words str))
      _ -> Nothing
    parseSymbs :: [String] -> Maybe [String]
    parseSymbs strings = case (length strings) of 
      0 -> Nothing
      otherwise -> Just strings
parseAlphabet _ = Nothing

--parse list of states
parseState :: String -> Maybe [State]
parseState str = case (length (lines str)) of
  1 ->
    let x = head (lines str) in
    case length x of 
      0 -> Nothing
      otherwise -> Just (words x)
  otherwise -> Nothing
--Program format: 1+ string, 1 - name of start state, 2.. - rule ::= "symb0 state0 -> symb1 [L/R/S] state1"
parseProg :: String -> Maybe (Status, Rules)
parseProg str = 
  let list_str = lines str in
  case length list_str of
    0 -> Nothing
    1 -> Nothing
    otherwise ->
      let s0 = head (list_str)
          rs_str = tail list_str in
      case (length (words s0), parseRules rs_str) of
        (1, Just rules) -> Just (Status [] 0 (head (words s0)) False, rules)
        _ -> Nothing
        where
          parseRule :: String -> Maybe Rule
          parseRule str = case length r of
                            2 -> case length r0 of
                                  2 -> case length r1 of { -- левая часть из 2 слов
                                          2 -> case lookup (r1 !! 0) (zip ["L","R","S"] [L,R,S]) of { -- левая и правая из 2 слов
                                                  Just dir -> Just (Rule (r0 !! 1) (r0 !! 0) "" (r1 !! 1) dir);
                                                  Nothing -> Nothing;};
                                          3 -> -- левая из 2, правая из 3
                                            case lookup (r1 !! 1) (zip ["L","R","S"] [L,R,S]) of { 
                                              Just dir -> Just (Rule (r0 !! 1) (r0 !! 0) (r1 !! 0) (r1 !! 2) dir);
                                              Nothing -> Nothing;};
                                          _ -> Nothing;}
                                  _ -> Nothing
                                 where r0 = words (r !! 0)
                                       r1 = words (r !! 1)
                            _ -> Nothing
                          where r = splitOn " -> " str
          mb_rule2rule :: Maybe Rule -> Rule
          mb_rule2rule (Just r) = r
          parseRules :: [String] -> Maybe [Rule]
          parseRules strs | isExistNothing (map parseRule strs) = Nothing
                          | True = Just (map mb_rule2rule (map parseRule strs))
isExistNothing :: [Maybe a] -> Bool
isExistNothing [] = False
isExistNothing (Nothing:xs) = True
isExistNothing (_:xs) = isExistNothing xs

parseInput :: String -> [String] -> Maybe [String]
parseInput s letters  | length(lines s)==1 && elem False (map (\w -> elem w letters) (words (head (lines s)))) = Nothing
                      | length(lines s)==1 = Just (words (head (lines s)))
                      | otherwise = Nothing

list2str :: [String] -> String
list2str l = foldr1 (\x y -> x ++ " " ++ y) l
rule2str :: Rule -> String
rule2str Rule{..} = case direct of
                      L -> start ++ "L" ++ end
                      R -> start ++ "R" ++ end
                      S -> start ++ "S" ++ end
                    where start = symb0 ++ " " ++ state0 ++ " -> " ++ symb1 ++ " "
                          end = " " ++ state1

rules2str :: Rules -> String
rules2str [] = ""
rules2str (r:rs) = (rule2str r) ++ "\n" ++ (rules2str rs)

check_symbols :: Rules -> [String] -> Maybe Int
check_symbols (Rule{..}:rules) letters | symb1=="" = if elem False (map (\w -> elem w letters) [symb0]) then Nothing else check_symbols rules letters
                                       | otherwise = if elem False (map (\w -> elem w letters) [symb0, symb1]) then Nothing else check_symbols rules letters
check_symbols [] _ = Just 1
